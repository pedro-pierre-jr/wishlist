from typing import List
from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

from . import crud,models,schemas,auth
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

## Lista de Requisições
@app.post("/wishlist/", response_model=schemas.Wish)
def criar_desejo(wish: schemas.WishCreate, db: Session = Depends(get_db)):
    return crud.create_wish(db=db, wish=wish)

@app.get("/wishlist/", response_model=List[schemas.Wish])
def get_desejos(db: Session = Depends(get_db)):
    return crud.get_wishlist(db)

@app.get("/wishlist/random/", response_model=schemas.Wish)
def get_desejo_aleatorio(db: Session = Depends(get_db)):
    return crud.retornar_id_aleatorio(db)
    

@app.get("/wishlist/{wish_id}/", response_model=schemas.Wish)
def get_desejo_id(wish_id: str, db: Session = Depends(get_db)):
    db_wishlist = crud.get_wishlist_by_id(db, wish_id=wish_id)
    if db_wishlist is None:
        raise HTTPException(status_code=404, detail="Item não está na lista de desejos!")
    return db_wishlist

@app.put("/wishlist/situacao/")
def put_situacao_desejo(wish: schemas.WishUpdateSituacao, db: Session = Depends(get_db)):
    return crud.atualiza_situacao_wishlist(db=db, wish=wish)


