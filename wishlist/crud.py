from sqlalchemy.orm import Session
from sqlalchemy.sql import text

from . import models, schemas
import uuid

def get_wishlist_by_id(db: Session, wish_id: str):
    return db.query(models.Wishlist).filter(models.Wishlist.id == wish_id).first()

def get_wishlist(db: Session):
    return db.query(models.Wishlist).all()

def create_wish(db: Session, wish: schemas.WishCreate):
    chave_id = str(uuid.uuid1())
    db_wishlist = models.Wishlist(
        id=chave_id, 
        titulo=wish.titulo,
        descricao=wish.descricao,
        link = wish.link,
        foto = wish.foto
    )
    db.add(db_wishlist)
    db.commit()
    db.refresh(db_wishlist)
    return db_wishlist

def atualiza_situacao_wishlist(db: Session, wish: schemas.WishUpdateSituacao):
    chave = wish.id
    #Verifica se existe um desejo com o id informado
    wishlist = get_wishlist_by_id(db, chave)
    db.query(models.Wishlist).filter(wishlist.id == chave).\
    update({"adquirido": "s"}, synchronize_session="fetch")   
    db.commit()
    return get_wishlist_by_id(db, chave)

def retornar_id_aleatorio(db: Session):
    string_sql = text("SELECT id FROM wishlist OFFSET floor(random() * (SELECT COUNT(*) FROM wishlist))LIMIT 1;")
    resultado = db.execute(string_sql).fetchall() # retorna uma lista de tuplas 
    lista_id = []
    for tupla in resultado:
        lista_id.append(tupla[0])
    return get_wishlist_by_id(db, lista_id[0])




